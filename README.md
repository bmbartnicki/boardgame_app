# Projekt Aplikacji do zarządzania grami planszowymi oraz organizowania wydarzeń opartych o posiadane gry planszowe
## Autorzy
Bartlomiej Bartnicki bartlomiej.bartnicki@microsoft.wsei.edu.pl

Pawel Drab pawel.drab@microsoft.wsei.edu.pl

Alicja Cwiek alicja.cwiek@microsoft.wsei.edu.pl
## Cel Projektu
### Wstęp
Celem projektu jest stworzenie aplikacji webowej dla entuzjastów gier planszowych.
Aplikacja będzie posiadała interfejs webowy. 

### Na czym chcemy zarabiać
Każdy użytkownik będzie posiadał bazę posiadanych przez siebie gier,
będzie mógł organizować i brać udział w wydarzeniach związanych z grami planszowymi,
których tworzenie i zarządzanie będzie wspomagane przez aplikację, 
w szczególności o bazę posiadanych gier.

Aplikacja będzie darmowa i wierzymy że dzięki swojej użyteczności pozyska grono użytkowników. 
Dzięki danym o posiadanych i granych grach przez użytkowników będziemy mogli nawiązać współpracę 
ze sklepami, które oferują gry planszowe oraz proponować użytkowników gry, które faktycznie ich zainteresują.
Drugim kanałem wpływów będzie  nawiązanie współpracy z firmami dostarczającymi jedzenie, o którym będziemy przypominać 
użytkownikiom przed eventami.

**W pierwszej wersji aplikacji nie zamierzamy implementować funkcionalności ukierunkowanych na zarabianie.**
Wymagają one konsultacji z podmiotami trzecimi, któym podczas negocjacji biznesowych chcemy pokazać
działający produkt, który posiada już użytkowników. 
Projektując i implementując aplikacji musimy pamiętać jednak o tych funkcionalnościach.

### Zarys funkcjonalności wersji 1.0
Dokłady opis funkcionalności znajduje się na zewnętrznej stronie, 
która jest automatycznie generowana na podstawie yamli z ich opisem.
https://bmbartnicki.gitlab.io/boardgame_app/index.html

1. Każdy użytkownik posiada bazę swoich egzemplarzy gier.
1. Egzemplarze gier są powiązane z bazą gier w aplikacji.
1. Użytkownicy mogą tworzyć wydarzenia na których będą grać w konkretne gry.
1. Gry na wydarzenie mogą być wybierane w formie ankiety przez uczestników.
1. Gry na wydarzenie i do ankiety mogą pochodzić z listy posiadanych gier uczetników.
1. Gracze mogą prowadzić handel handel barterowy swoimi egzemplarzami.
1. Aplikacja zapewni podstawowe funkcje społecznościowe wzorowane na innych aplikacjach.
1. Aplikacja NIE będzie drugim Facebookiem.

### Technologie
#### Backend 
Aplikacja będzie oparta o framework Django oraz Python3.
- Python3.6
- Djnago
- social-auth-app-django (FB, Google auth)
- nltk (for search engine)
- docker 

<img src="https://www.python.org/static/opengraph-icon-200x200.png"  width="120" height="120">
<img src="https://cdn-images-1.medium.com/max/1200/1*1OBwwxzJksMv0YDD-XmyBw.png"  width="120" height="120">
<img src="https://www.docker.com/sites/default/files/Whale%20Logo332_5.png"  width="120" height="120">

#### Frontend
- Vue.js
- Bootstrap
- JQuery 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/1200px-Vue.js_Logo_2.svg.png"  width="120" height="120">
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4aNQsCVu1DUDsJN520DruhJKZr8HfaBvXO4-l4i4pyC-DwJVEcCl-Oc8"  width="120" height="120">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Boostrap_logo.svg/220px-Boostrap_logo.svg.png"  width="120" height="120">

#### Komunikacja Backend-Frontend
Komunikacja ta będzie oparta o architekturę REST.
Nacisk będzie położony na to aby backend dostarczał 
dane w takiej formie aby Vue.js mógł je wyświetlić bez 
dodatkowego przetwarzania.

#### Kontrola Wersji
Kontrola wersji plików projektu będzie realizowana przez
GITa. 

Zastosowana zostanie prosta strategia gałęzi, w której 
każdy feature będzie tworzony na podgałęzi, która po zakończeniu
implementacji featera będzie mergowana do gałęzi głównej.

<img src="https://nvie.com/img/fb@2x.png"  height="400">


#### DevOps
Projekt będzie realizowany w duchu DevOps. 
Każda zmiana w kodzie będzie wyzwalała uruchomienie
skryptów, które będą testować kod statycznie i 
funkcjonalnie oraz budować kontener z aplikacją, 
który będzie gotowym produktem przygoyowanym pod
wdrożenie na produkcję.

Środowisko oraz konfiguracja zgodnie z zasadami,
"Environment as a code" oraz "Configuration as a code"
będą utrzymywane w tym samym repozytorium co kod aplikacji,
Będą one zapisane, głównie w formie yamli, a tam gdzie
nie będzie to możliwe w postaci skryptów.

Ze względu na mały rozmiar aplikacji i początkową jego fazę
świadomi rezygnujemy z monitoringu aplikacji. Pozostałe 
elementy cyklu wytwarzania w oparciu o DevOpsowe podejście
zostaną zastosowane. 

<img src="https://cdn-images-1.medium.com/max/1200/1*CSZxfOMlVsKsrMkqTxFiMQ.png"  height="400">


#### Gitlab
Projekt będzie prowadzony w Gitlabie, który zapewnia między innymi:
- pipeline  
- CI
- CD 
- wspracie dla kontenerów (Docker)
- taskownicę 
- system review kodu
- wsparcie komunikacji 

![img](https://docs.gitlab.com/ee/ci/img/cicd_pipeline_infograph.png)

## Analiza konkurencji:
Na rynku dostępnych jest kilka portali i aplikacji które służą do zarządzania kolekcją gier planszowych oraz 

### BoardGameGeek.com
Duża społeczność, masa gier, wielkie forum, sklep, dużo solucji, poradników itp. 
Mając profil można kolekcjonować gry. Wady - brzydkie to jak noc, chociaż jest też nowa wersja witryny, 
dopasowana do urządzeń mobilnych. Forum z wymianą wygląda strasznie i jest mocno toporne. 
Kiedy zapycha się jeden wątek, zakładany jest kolejny. Wersja 31 dla polskiej grupy, 
założona we wrześnu 2018: https://boardgamegeek.com/geeklist/245712/31-polski-mathandel-polish-math-trade . 

### Board Game Collections
 
Słabe oceny, płatna, około 100 instalacji.Głównie do budowania swojej kolekcji gier. 
Fajna opcja ze skanowaniem kodu kreskowego pudełka. 
Firma, która ją wydała ma masę appek do spisywania kolekcji (pociągi, bilety, książki). 
Są dostępni na AppStore i Google Play

### Board Game Bible  
Wygląda to bardzo fajnie - wysoki rating, ładna grafika, oceny, sortowanie, recenzje, wishlist, 
duża baza danych, synchronizacja z BoardGameGeek.com, wygląda na darmową. 
Nie znaleziono nic o wymianie gier i organizowaniu eventów.  

### LiQD https://www.liqdbg.com/
Strona jest strasznie ciężka i długo się wczytuje. Można importować swoją kolekcję, 
filtrować ją, oznaczać itemy do sprzedaży i szukać gier do kupienia. 

### Board Game Stats
Bardziej do spisywania swoich statystyk w grach, ale jest też możliwość ich katalogowania, 
wrzucaniu wydarzeń na BGG.com, rankingi graczy itp. 

### Masa aplikacji on-premise 
Większość wygląda jak walka z windowsowymi User Formami. Dużo z nich nie jest 
dedykowana grom planszowym tylko ogólnie pojętemu kolekcjonerstwu. 
Przykłady: eXtreme Games Manager, GCstar, MediaDB, DataCrow. 

## Analiza Rynku

### Rynek gier planszowych 
Niestety materiałów źródłowych jest mało, a liczby są rzucone bez żadnych odnośników i lat których dotyczą. 
W większości artykułów zaznacza się, że rynek się prężnie rozwija oraz, 
że przechodzi z fazy rozrywki „tylko dla nerdów” w zabawę dla osób z mniejszym doświadczeniem. 
Za lidera w Europie uznaje się Niemcy – to kraj, w którym wydaje się najwięcej nowych tytułów, 
najwięcej osób gra w planszówki i karcianki, tutaj również odbywa się największy branżowy event - Spiel Essen. 
Rynek polski rośnie dosyć szybko. Bardzo popularne są u nas gry imprezowe, 
gdzie nie jest wymagana znajomość skomplikowanych reguł. Planszówki stały się modnym 
prezentem urodzinowym i weselnym. Polscy konsumenci angażują się w akcje na kickstarterze 
(obecnie 92 projekty, wiele anglojęzycznych). 
Wartość rynku:
- Świat – (2012) 3,4 mld USD
- Niemcy – (2013) 375 mln EUR, co roku ok. 900 nowych tytułów
- Francja – 320 mln EUR, 400 nowych tytułów rocznie
- Wielka Brytania – 300 mln EUR
- Rosja – (2011) 70 mln USD
- Polska – 400 mln PLN, wydaje się ok 400 tytułów rocznie, wzrost rok do roku około 15-20%

#### Kanały na YouTube czyli miejsca, gdzie moglibyśmy się reklamować:
- The Dice Tower – 206 tys subskrypcji
- Shut Up & Sit Down – 149 tys
- Watch It Played – 137 tys
- BoardGameGeek – 68 tys
- rahdo – 81 tys
- TheGameBoyGeek – 22 tys
- GameTrollTV (PL) – 27 tys
 

#### Blogi o grach planszowych – kolejne miejsce reklamy:
[PL]
- https://dicelandblog.pl
- http://powermilk.pl
- https://boardtime.pl
- http://www.przystole.org
- http://blogiplanszowkowe.blogspot.com
- http://planszoholik.pl
- https://gryplanszowe.pl

[EN]
- https://boardgamegeek.com/blog/1
- https://www.dicetower.com
- https://www.unboxedtheboardgameblog.com
- https://tabletopgamesblog.com
- https://www.boardgamequest.com
- http://www.everythingboardgames.com
- https://www.theboardgamefamily.com

#### Eventy
[Świat]:
- Internationale Spieltage – Essen (Niemcy) – 1000 wystawców z 50 krajów
- MeepleCon – Melbeourne (Australia)
- SaltCon – Utah (USA)
- Board games decathlon – Bukareszt (Rumunia)
- Lucca Comics and Games – Lucca (Włochy) – 500 eventów, 20 wystawców
- Spielwies’n – Monachium (Niemcy)

[PL]:
- Pyrkon – Poznań (44 tys uczestników, festiwal fantastyki, gier planszowych i karcianych),
- Planszówki na Narodowym – Warszawa,
- Grativlavia,
- Pionek – Zabrze ,
- Łódzki Port Gier – Łudź, 566 gier w 2018
- Polcon – Toruń, najstarszy polski konwent
- Gramy – Trójmiasto

 
#### Polscy wydawcy – wydawnictwa i sklepy, które mogłyby być zainteresowane współpracą z nami:
Na polskim rynku działa około 60 wydawców gier planszowych. Najważniejsi z nich to:
- ADAMIGO - http://www.adamigo.pl/kontakt/
- AXEL - http://www.axel.pl/kontakt.html
- BARD - http://wydawnictwo.bard.pl/kontakt/
- Black Monk - http://munchkin.pl/kontakt/
- CUBE FACTORY OF IDEAS - http://factorycube.pl/
- EGMONT - http://www.krainaplanszowek.pl/
- FABRYKA GIER HISTORYCZNYCH - http://www.wydawnictwofgh.pl/
- G3 - http://www.g3poland.com/
- GALAKTA - http://www.forum.galakta.pl/
- GALMADRIN - http://www.galmadrin.com/
- GRANNA - http://www.granna.pl/kontakt
- GRY LEONARDO - http://www.gryleonardo.pl/
- HOBBITY.EU - http://www.hobbity.eu/
- KUŹNIA GIER - http://www.kuzniagier.pl/
- LACERTA - http://www.lacerta.pl/component/
- LET'S PLAY - http://letsplaygames.pl/
- LOCWORKS - http://www.locworks.pl/
- NUKEDAMAGE - http://nukedamage.pl/ PENTAKL - http://www.pentakl.pl/
- PHALANAX GAMES POLSKA - http://www.phalanxgames.pl/
- PIATNIK - http://www.piatnik.pl/
- POLSKIE GRY PLANSZOWE - http://www.polskiegryplanszowe.pl/
- PORTAL - http://www.wydawnictwoportal.pl/kontakt/
- REBEL - http://wydawnictwo.rebel.pl/
- RED IMP - http://www.redimp.pl/
- SPAWN - http://wladcaareny.pl/
- TREFL - http://trefl.com/

#### Przydatne linki (źródła):
- http://www.een.org.pl/index.php/rynki-zagraniczne-archiwum/blind_style/1/page/34/articles/miedzynarodowy-rynek-gier-planszowych.html
- https://www.polskieradio.pl/42/1699/Artykul/2107530,Gry-planszowe-warte-w-Polsce-400-mln-zl
- http://www.festiwalgramy.com/site/pages/wypozyczalnia
- https://www.newsweek.pl/polska/najpopularniejsze-gry-planszowe/d7qsqnb
- https://oveit.com/blog/2016/10/12/best-board-games-events-and-conventions-in-the-world/

## Materiały
https://drive.google.com/open?id=1ytPPvEjL7JWVAsAy2a7Qly4IQOHoq9Lj
1. Notatki ze spotkań
1. Zarys featerów



## Zależności pomiędzy Epicami

Epic Event zawiera tylko te pod Epicki, które są niezależne od wszystkiego poza Userami.

Games to to samo Games_, dodane dla poprawienia czytelności

Users_&_Games to połączenie Users, Games Epic, dodane dla poprawienia czytelności

Wykres ten zawiera oczywiste zależności, jednak w trakcie implementacji mogą wyniknąć kolejne.
Będziemy się starać jednak ich unikać. Zależności cykliczne mogą świadczyć o błędnej architekturze. 

```mermaid
graph LR;
  Users--> Friends
  Users --> Events
  Users_Games --> Privacy_Management
  Users --> Sync_with_FB
  Users_&_Games --> Users_Games
  Users_&_Games --> Favorite_Games
  Users_&_Games --> Games_Market
  Users_&_Games --> Games_Ranking
  Users --> Biznes_Users

  Events --> Events_With_Game_Voting
  Events--> Privacy_Management
  Users -.-> Users_&_Games 
  Games -.-> Users_&_Games 

  Friends--> Report
  Favorite_Games --> Report
  Events --> Report
  Users_Games --> Report
  Games_Market --> Report
  Games_Ranking --> Report
  Games_ --> Events_With_Game_Voting
  Blog
```