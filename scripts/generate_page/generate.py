from yaml import load
from operator import itemgetter, attrgetter, methodcaller
from collections import Counter
from glob import glob


def open_and_read(path):
    # path = path.replace("scripts/generate_page/", "")
    with open(path) as file:
        return file.read()


class PageBuilder:
    page_template = open_and_read("scripts/generate_page/page.html")
    progress_template = open_and_read("scripts/generate_page/progress.html")
    user_stories_templates = open_and_read("scripts/generate_page/userstory.html")
    acceptance_criteria_template = open_and_read("scripts/generate_page/acceptancecriteria.html")
    epic_template = open_and_read("scripts/generate_page/epic.html")
    link_template = open_and_read("scripts/generate_page/link.html")
    user_stories_statistic = Counter()
    statuses_order = ["DONE", "DOING", "TODO"]
    bootstrap_classes_mapping = {'DOING': 'primary',
                                 'DONE': 'success',
                                 'TODO': 'danger'}

    def __init__(self, path_to_reqs, path_to_links):
        self.path_to_reqs = path_to_reqs
        self.path_to_links = path_to_links
        self.user_stories_output = ""
        self.progress_output = ""
        self.links_output = ""
        self.epics_output = ""
        self.current_sprint_epics_ids = []

    def __get_list_of_files_by_pattern(self, pattern):
        return glob(pattern)

    def with_epics(self):
        for file in self.__get_list_of_files_by_pattern(self.path_to_reqs):
            reqs_dict = load(open(file).read())
            if reqs_dict['STATUS'] == "DOING":
                self.current_sprint_epics_ids.append("E" + str(reqs_dict['ID']))
            self.epics_output += self.epic_template.format(
                epic_name=reqs_dict['NAME'],
                id=reqs_dict['ID'],
                note=reqs_dict['NOTES'],
                desc=reqs_dict['DESC'],
                status=reqs_dict['STATUS'],
                epic_status_boostrap_class=self.bootstrap_classes_mapping[reqs_dict['STATUS']]
            )
        return self

    def with_user_stories(self):
        uses = sorted(self.__get_us(),
                      key=itemgetter('STATUS_NUM', "PRIORITY"),
                      reverse=True)
        for us in uses:
            self.user_stories_statistic += Counter({us['STATUS']: 1})
            ac_output = "\n".join([
                self.acceptance_criteria_template.format(
                    us_id=us["ID"],
                    ac_id=ac_id,
                    ac_name=ac_name)
                for ac_id, ac_name in us["AC"].items()])
            self.user_stories_output += \
                self.user_stories_templates.format(
                    ac_body=ac_output,
                    priority=us['PRIORITY'],
                    status=us['STATUS'],
                    status_boostrap_class=self.bootstrap_classes_mapping[us['STATUS']],
                    epic_status_boostrap_class=self.bootstrap_classes_mapping[us['EPIC_STATUS']],
                    id=us['ID'],
                    name=us['NAME'],
                    notes=us["NOTES"],
                    epic_name=us['EPIC_NAME'],
                    epic_id=us['EPIC_ID'])
        return self

    def __get_us(self):
        return [us for file in self.__get_list_of_files_by_pattern(self.path_to_reqs)
                for us in self.__get_us_from_file(file)]

    def with_progress(self):
        todo = self.user_stories_statistic['TODO']
        doing = self.user_stories_statistic['DOING']
        done = self.user_stories_statistic['DONE']
        sum = todo + doing + done
        self.progress_output = self.progress_template.format(
            todo=todo,
            doing=doing,
            done=done,
            todo_in_percent=todo / sum * 100,
            doing_in_percent=doing / sum * 100,
            done_in_percent=done / sum * 100)
        return self

    def with_links(self):
        self.links_output = "\n".join([
            self.link_template.format(name=name, link=link)
            for name, link in load(open(self.path_to_links).read())['links'].items()
        ])
        return self

    def save(self, path_to_output):
        with open(path_to_output, 'w') as f:
            f.write(self.page_template.format(
                user_stories=self.user_stories_output,
                epics=self.epics_output,
                progress=self.progress_output,
                links=self.links_output,
                current_sprint_epics=",".join(self.current_sprint_epics_ids)
            ))

    def __get_us_from_file(self, file):
        reqs_dict = load(open(file).read())
        epic_name = reqs_dict['NAME']
        epic_id = reqs_dict['ID']
        epic_status = reqs_dict['STATUS']
        print(reqs_dict['USER STORIES'], file)
        user_stories = reqs_dict['USER STORIES'].items()
        user_stores = [{
            **value,
            "NAME": key,
            "EPIC_NAME": epic_name,
            "EPIC_ID": epic_id,
            "EPIC_STATUS": epic_status,
            "STATUS_NUM": self.statuses_order.index(value['STATUS'])
        }
            for key, value in user_stories
        ]
        return user_stores


pb = PageBuilder(path_to_reqs="docs/req*",
                     path_to_links="docs/links.yaml")

# pb = PageBuilder(path_to_reqs="../../docs/req*",
#                  path_to_links="../../docs/links.yaml")

pb.with_epics(). \
    with_user_stories(). \
    with_links(). \
    with_progress(). \
    save(path_to_output="index.html")
