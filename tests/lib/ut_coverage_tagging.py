UT_COV_REQ_FILE = "tmp_ut_cov_req"
import os
from sys import r


def tag(f):
    f.__name__
    def _tag(string_with_tags):
        with open(UT_COV_REQ_FILE, "w+") as f:
            f.write(" " + string_with_tags)

def reset_coverage():
    os.remove(UT_COV_REQ_FILE)


def requirements_coverage():
    with open(UT_COV_REQ_FILE) as f:
        return set(f.read().split(" "))
